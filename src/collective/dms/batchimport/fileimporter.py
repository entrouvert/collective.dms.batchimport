# -*- coding: utf8 -*-

import logging
import os
import os.path

from email.mime.base import MIMEBase
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText

from zope import schema

from zope.component import queryUtility
from five import grok
from Products.CMFPlone.interfaces import IPloneSiteRoot
from Products.CMFCore.utils import getToolByName

import z3c.form.button
from plone import api
from plone.directives import form
from plone.i18n.normalizer.interfaces import IIDNormalizer

from plone.namedfile.field import NamedFile, NamedBlobFile

from . import _
from . import utils

log = logging.getLogger('collective.dms.batchimport')

class IImportFileFormSchema(form.Schema):
    file = NamedBlobFile(title=_(u"File"))

    title = schema.Text(title=_(u"title"), required=False)
    portal_type = schema.Text(title=_(u"portal_type"), required=False)
    location = schema.Text(title=_(u"location"), required=False)
    owner = schema.Text(title=_(u"owner"), required=False)
    treating_groups = schema.Text(title=_(u"treating_groups"), required=False)  # new, comme separated identifiers
    recipient_groups = schema.Text(title=_(u"recipient_groups"), required=False)  # new, comme separated identifiers
    keywords = schema.Text(title=_(u"keywords"), required=False)  # comma separated identifiers

    notification_recipients = schema.Text(title=_(u"notification_recipients"), required=False)  # comma separated emails

    transitions_to_apply = schema.Text(title=_(u"transitions_to_apply"), required=False)

    # legacy
    treating_group = schema.Text(title=_(u"treating_group"), required=False)


class ImportFileForm(form.SchemaForm):
    schema = IImportFileFormSchema

    # Permission required to
    grok.require("cmf.ManagePortal")

    ignoreContext = True

    grok.context(IPloneSiteRoot)

    # appear as @@fileimport view
    grok.name("fileimport")

    def get_folder(self, foldername):
        folder = getToolByName(self.context, 'portal_url').getPortalObject()
        for part in foldername.split('/'):
            if not part:
                continue
            folder = getattr(folder, part)
        return folder

    def convertTitleToId(self, title):
        """Plug into plone's id-from-title machinery.
        """
        #title = title.decode('utf-8')
        newid = queryUtility(IIDNormalizer).normalize(title)
        return newid

    @z3c.form.button.buttonAndHandler(_('Import'), name='import')
    def import_file(self, action):
        # Extract form field values and errors from HTTP request
        data, errors = self.extractData()
        if errors:
            self.status = self.formErrorsMessage
            return

        portal_type = data['portal_type']
        filename = data['file'].filename
        owner = data['owner']
        if data['treating_group']:  # legacy
            treating_groups = set([data['treating_group']])
        else:
            treating_groups = set([x.strip() for x in (data['treating_groups'] or '').split(',')])

        recipient_groups = set([x.strip() for x in (data['recipient_groups'] or '').split(',')])

        folder = self.get_folder(data['location'])
        keywords = set([x.strip() for x in (data['keywords'] or '').split(',')])

        document_id = self.convertTitleToId(data.get('title') or os.path.splitext(filename)[0])

        document = utils.createDocument(self, folder, portal_type, document_id,
                filename, data['file'], owner,
                treating_groups=treating_groups,
                recipient_groups=recipient_groups,
                metadata={
                    'title': data.get('title'),
                    'keywords': keywords,
                    })

        if data.get('transitions_to_apply'):
            transitions = data.get('transitions_to_apply').split(",")
            for transition in transitions:
                api.content.transition(document, transition)
            document.reindexObject(idxs=['review_state', 'allowedRolesAndUsers'])

        if data.get('notification_recipients'):
            document_location = self.request.response.headers['location']
            subject = 'Nouveau document, %s' % data.get('title')
            message = 'Ouvrir dans la GED :\n  %s\n' % document_location
            recipients = data.get('notification_recipients').split(',')
            self.send_message(subject, message, recipients)

    def send_message(self, subject, message, recipients):
        msg = MIMEMultipart()
        msg['Subject'] = subject
        msg['To'] = ', '.join(recipients)
        msg['From'] = api.portal.get().getProperty('email_from_address') or 'admin@localhost'

        msg.attach(MIMEText(message, _charset='utf-8'))
        self.context.MailHost.send(msg.as_string())
